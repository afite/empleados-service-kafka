package profe.empleados.negocio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import profe.empleados.daos.EmpDAO;
import profe.empleados.model.Empleado;
import profe.empleados.sagas.EmpleadosEventProducer;

@Service
public class EmpNegocioImpl implements EmpNegocio {
	
	@Autowired
	private EmpDAO dao;
	
	@Autowired
	private EmpleadosEventProducer eventsProducer;

	public Empleado getEmpleado(String cif) {
		return dao.getEmpleado(cif);
	}

	public List<Empleado> getAllEmpleados() {
		return dao.getAllEmpleados();
	}

	public boolean insertaEmpleado(Empleado emp) {
		boolean bOk = dao.insertaEmpleado(emp);
		if (bOk) {
//			for (int i=0; i<3; i++)
				eventsProducer.sendCreateEmpleadoEvent(emp);
		}
		return bOk;
	}

	public boolean modificaEmpleado(Empleado emp) {
		return dao.modificaEmpleado(emp);
	}

	public boolean eliminaEmpleado(String cif) {
		boolean bResult = dao.eliminaEmpleado(cif);
		if (bResult) {
			eventsProducer.sendDeleteEmpleadoEvent(cif);
		}
		return bResult;
	}

}
