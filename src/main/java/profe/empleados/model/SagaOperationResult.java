package profe.empleados.model;


import java.io.Serializable;

public enum SagaOperationResult implements Serializable  {
	COMMIT, ROLLBACK
}
